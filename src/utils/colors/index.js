const mainColors = {
  purple: '#564AA5',
  blue: '#87F0F9',
  yellow: '#FBC263',
  red: '#EE8C84',
  white: '#F3F5F8',
};

export const colors = {
  warning: mainColors.red,
  white: 'white',
  background: {
    primary: mainColors.purple,
    secondary: mainColors.blue,
    icon: mainColors.yellow,
    genre: mainColors.red,
  },

  text: {
    primary: mainColors.purple,
    secondary: mainColors.blue,
    rating: mainColors.white,
    genre: mainColors.yellow,
    description: mainColors.purple,
  },
  button: {
    primary: {
      background: mainColors.blue,
      text: 'white',
    },
    secondary: {
      background: mainColors.green,
      text: 'white',
    },
  },

};
