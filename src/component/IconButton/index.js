import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import {
  ICBack, ICCheck, ICLike, ICLogout, ICShare,
} from '../../assets';
import { colors } from '../../utils';

function Icon({ iconHeight, iconWidth, type }) {
  if (type === 'Back') return <ICBack />;
  if (type === 'Like') return <ICLike />;
  if (type === 'Share') return <ICShare />;
  if (type === 'LogOut') return <ICLogout height={iconHeight} width={iconWidth} />;
  if (type === 'Check') return <ICCheck height={iconHeight} width={iconWidth} />;
  return <ICBack />;
}
function IconButton({
  onPress, nonButton, iconHeight, iconWidth, type,
}) {
  return (
    <View>
      {nonButton ? (
        <View style={styles.iconNonButton}>
          <View style={styles.icon}>
            <Icon iconHeight={iconHeight} iconWidth={iconWidth} type={type} />
          </View>
        </View>
      ) : (
        <TouchableOpacity style={styles.iconWrapper} onPress={onPress}>
          <View style={styles.icon}>
            <Icon iconHeight={iconHeight} iconWidth={iconWidth} type={type} />
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
}

export default IconButton;

const styles = StyleSheet.create({
  iconWrapper: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    backgroundColor: colors.background.icon,
    opacity: 0.8,
    justifyContent: 'center',
  },
  icon: {
    padding: 16,
    alignSelf: 'center',
    borderRadius: 8,
  },
  iconNonButton: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    backgroundColor: colors.background.icon,
    opacity: 0.8,
    justifyContent: 'center',
  },
});
