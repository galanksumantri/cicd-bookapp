import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { colors, fonts } from '../../utils';

export default function LinkComponent({
  title, size, align, onPress,
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text(size, align)}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  text: (size, align) => ({
    fontFamily: fonts.primary[800],
    fontSize: size,
    color: colors.text.secondary,
    textAlign: align,
    marginTop: 10,
  }),
});
