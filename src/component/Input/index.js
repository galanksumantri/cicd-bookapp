import React, { useState } from 'react';
import {
  StyleSheet, TextInput, View,
} from 'react-native';
import { colors } from '../../utils';

export default function Input({
  value, onChangeText, secureTextEntry, keyboardType,
  placeholder, marginTopInput, marginBottomInput,
}) {
  const [border, setBorder] = useState(colors.border);
  const onFocusForm = () => {
    setBorder(colors.text.rating);
  };
  const onBlurForm = () => {
    setBorder(colors.text.secondary);
  };
  return (
    <View style={{ marginTop: marginTopInput, marginBottom: marginBottomInput }}>
      <TextInput
        style={styles.input(border)}
        onFocus={onFocusForm}
        onBlur={onBlurForm}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        keyboardType={keyboardType}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  input: (border) => ({
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
    padding: 12,
  }),

});
