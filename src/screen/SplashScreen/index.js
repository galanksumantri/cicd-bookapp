import {
  StyleSheet, Text, View, StatusBar, ImageBackground, Dimensions,
} from 'react-native';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { LogoBook } from '../../assets';
import { colors, fonts } from '../../utils';

function SplashScreen({ navigation }) {
  const getToken = useSelector((state) => state.Auth.token);
  useEffect(() => {
    setTimeout(() => {
      if (getToken) {
        navigation.replace('HomeTabScreen');
      } else {
        navigation.replace('LoginScreen');
      }
    }, 3000);
  }, []);
  return (
    <View style={styles.page}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      <ImageBackground style={styles.image} source={LogoBook} />
      <Text style={styles.title}>GaBook</Text>
      <Text style={styles.nickname}>Galang Sumantri</Text>
    </View>
  );
}

export default SplashScreen;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.background.primary,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    color: colors.text.rating,
    marginTop: 10,
    fontFamily: fonts.primary[800],
  },

  image: {
    height: windowHeight * 0.16,
    width: windowWidth * 0.24,
  },

  nickname: {
    fontSize: 14,
    color: colors.text.primary,
    fontFamily: fonts.primary[800],
    position: 'absolute',
    bottom: 19,
  },
});
