import React, { useEffect } from 'react';
import {
  Alert,
  RefreshControl,
  ScrollView, StatusBar, StyleSheet, Text, View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Card, IconButton, Loading } from '../../component';
import {
  getDataBooksPopular, getDataBooksRecommended, logout, refresh,
} from '../../redux';
import { colors, fonts } from '../../utils';
import { recommendedSort } from '../../utils/helperFunction';

function HomeTabScreen({ navigation }) {
  const dispatch = useDispatch();
  const dataBooksPopular = useSelector((state) => state.dataBooks.booksPopular);
  const dataBooksRecommended = useSelector((state) => state.dataBooks.booksRecommended);
  const getToken = useSelector((state) => state.Auth.token);
  const isRefreshing = useSelector((state) => state.dataBooks.isRefreshing);
  const isLoading = useSelector((state) => state.dataBooks.isLoading);

  useEffect(() => {
    dispatch(getDataBooksRecommended(getToken, 6));
    dispatch(getDataBooksPopular(getToken));
  }, []);

  const onRefresh = () => {
    dispatch(refresh(true));
    dispatch(getDataBooksRecommended(getToken, 6));
    dispatch(getDataBooksPopular(getToken));
  };

  const logOut = () => {
    Alert.alert(
      'Peringatan',
      'Apakah anda yakin ingin Logout?',
      [
        {
          text: 'Tidak',
        },
        {
          text: 'Ya',
          onPress: () => {
            dispatch(logout());
            navigation.replace('SplashScreen');
          },
        },
      ],
    );
  };
  return (
    <View style={styles.page}>
      {
        isLoading ? <Loading /> : (
          <View style={{
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}
          >
            <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
            <ScrollView
              showsVerticalScrollIndicator={false}
              refreshControl={(
                <RefreshControl
                  refreshing={isRefreshing}
                  onRefresh={() => onRefresh()}
                />
              )}
            >

              <View style={styles.header}>
                <Text style={styles.greeting}>Good Morning, Axel</Text>
                <IconButton iconHeight={30} iconWidth={30} type="LogOut" onPress={() => logOut()} />
              </View>

              <Text style={styles.label}>Recommended</Text>
              <View style={styles.recommended}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {
                  dataBooksRecommended.sort(recommendedSort).map((item) => (
                    <View style={{ marginHorizontal: 3 }} key={item.id}>
                      <Card
                        onPress={() => navigation.navigate('BookDetailScreen', { id: item.id })}
                        judul={item.title}
                        author={item.author}
                        publisher={item.publisher}
                        source={{ uri: item.cover_image }}
                        harga={item.price}
                        rating={item.average_rating}
                      />
                    </View>
                  ))
                }
                </ScrollView>
              </View>

              <Text style={styles.label}>Popular Book</Text>
              <View style={styles.popular}>
                {dataBooksPopular.map((item) => (
                  <View style={{ marginHorizontal: 3 }} key={item.id}>
                    <Card
                      onPress={() => navigation.navigate('BookDetailScreen', { id: item.id })}
                      judul={item.title}
                      author={item.author}
                      publisher={item.publisher}
                      source={{ uri: item.cover_image }}
                      harga={item.price}
                      rating={item.average_rating}
                    />
                  </View>

                ))}
              </View>
            </ScrollView>

          </View>
        )
      }
    </View>
  );
}

export default HomeTabScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,

  },
  greeting: {
    fontSize: 18,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
  },
  label: {
    color: colors.text.primary,
    fontFamily: fonts.primary[700],
    fontSize: 18,
  },
  recommended: {
  },

  popular: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

});
