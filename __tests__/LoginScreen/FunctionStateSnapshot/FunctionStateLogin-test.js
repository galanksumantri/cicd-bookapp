import React from 'react';
import {
  shallow, configure,
} from 'enzyme';
import renderer from 'react-test-renderer';
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
import Adapter from 'enzyme-adapter-react-16';
import LoginScreen from '../../../src/screen/LoginScreen';

configure({ adapter: new Adapter(), disableLifecycleMethods: true });

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
jest.mock('react-native-share', () => ({}));
jest.mock('react-redux', () => ({ useSelector: jest.fn(), useDispatch: () => mockDispatch }));

const mockDispatch = jest.fn();

const loginWrapper = shallow(<LoginScreen />);

describe('login screen testing', () => {
  it('should render correctly', () => {
    const tree = renderer.create(<LoginScreen />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('Check component', () => {
    it('should have an email field', () => {
      expect(loginWrapper.find('Input[testID="input-email"]').exists());
    });
    it('should have a password field', () => {
      expect(loginWrapper.find('Input[testID="input-password"]').exists());
    });
  });

  describe('Check function', () => {
    it('check component', () => {
      loginWrapper.find('Input[testID="input-email"]').simulate('changeText', { target: { value: 'myUser' } });
      loginWrapper.find('Input[testID="input-password"]').simulate('changeText', { target: { value: 'myPassword' } });
      loginWrapper.find('[testID="btn-login"]').simulate('press');
    });
  });
});
