import React from 'react';
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import HomeTabScreen from '../../../src/screen/HomeTabScreen';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
jest.mock('react-native-share', () => ({
}));
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

const mockDispatch = jest.fn();
configure({ adapter: new Adapter(), disableLifecycleMethods: true });

describe('HomeTabScreen', () => {
  it('should renders correctly', () => {
    jest.useFakeTimers();
    const tree = shallow(<HomeTabScreen />);
    expect(tree).toMatchSnapshot();
  });

  it('should renders `HomeTab Screen` module correctly', () => {
    expect(HomeTabScreen).toMatchSnapshot();
  });
});
